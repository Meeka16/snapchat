package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "B6652AD4-E0DB-21C7-FF1B-5670CC6A3800";
    public static final String SECRET_KEY = "90A4612E-86A9-0451-FF4C-9F8989D6A800";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
            LoginMenuFragment loginMenu = new LoginMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginMenu).commit();
        } else {
            MainMenuFragment instasnaps = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, instasnaps).commit();
        }
    }
}

