package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FindFriends extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FindFriendsFragment findFriends = new FindFriendsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, findFriends).commit();
    }
}
